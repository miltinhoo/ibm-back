package com.co.ibm.transformer;

import java.util.HashSet;
import java.util.Set;

import com.co.ibm.entity.Consumo;
import com.co.ibm.entity.Tarjeta;
import com.co.ibm.request_model.ConsumoRequest;

public class ConsumoTransformer {
	
	public static Consumo consumoRequestToConsumo(ConsumoRequest consumoRequest, Tarjeta tarjeta) {
		Consumo consumo = new Consumo();
		consumo.setDescripcion(consumoRequest.getDescripcion());
		consumo.setFecha(consumoRequest.getFecha());
		consumo.setMonto(consumoRequest.getMonto());
		consumo.setTarjeta(tarjeta);
		return consumo;
	}
	
	public static Set<ConsumoRequest> getConsumoResponse(Iterable<Consumo> consumosIterator) {	
		HashSet<ConsumoRequest> consumosResponse = new HashSet<>();
		
		consumosIterator.forEach(consumo -> {
			ConsumoRequest consumoRequest = new ConsumoRequest();
			consumoRequest.setDescripcion(consumo.getDescripcion());
			consumoRequest.setFecha(consumo.getFecha());
			consumoRequest.setMonto(consumo.getMonto());
			consumoRequest.setNumeroTarjeta(consumo.getTarjeta() != null ?
					consumo.getTarjeta().getNumeroTarjeta() : null);
			
			consumosResponse.add(consumoRequest);
		});
		
		return consumosResponse;
	}

}
