package com.co.ibm.transformer;

import java.util.HashSet;
import java.util.Set;

import com.co.ibm.entity.Cliente;
import com.co.ibm.entity.Tarjeta;
import com.co.ibm.request_model.TarjetaRequest;

public class TarjetaTransformer {
	public static Tarjeta tarjetaRequestToTarjeta(TarjetaRequest tarjetaRequest, Cliente cliente) {
		Tarjeta tarjeta = new Tarjeta();
		tarjeta.setCcv(tarjetaRequest.getCcv());
		tarjeta.setNumeroTarjeta(tarjetaRequest.getNumeroTarjeta());
		tarjeta.setTipo(tarjetaRequest.getTipo());
		tarjeta.setCliente(cliente);
		return tarjeta;
	}
	
	public static Set<TarjetaRequest> getTarjetaResponse(Iterable<Tarjeta> tarjetaIterable) {	
		HashSet<TarjetaRequest> tarjetaResponse = new HashSet<>();
		
		tarjetaIterable.forEach(tarjeta -> {
			TarjetaRequest tarjetaRequest = new TarjetaRequest();
			tarjetaRequest.setCcv(tarjeta.getCcv());
			tarjetaRequest.setNumeroTarjeta(tarjeta.getNumeroTarjeta());
			tarjetaRequest.setTipo(tarjeta.getTipo());
			tarjetaRequest.setIdCliente(tarjeta.getCliente() != null ? tarjeta.getCliente().getIdCliente() : null);
			tarjetaResponse.add(tarjetaRequest);
		});
		
		return tarjetaResponse;
	}
}
