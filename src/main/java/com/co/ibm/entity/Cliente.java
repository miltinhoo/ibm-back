package com.co.ibm.entity;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CLIENTE")
public class Cliente {
	
	@Id
    @GeneratedValue
    @Column(name = "id_cliente", nullable = false)
    private Long idCliente;
 
    @Column(name = "nombre", length = 50, nullable = false)
    private String nombre;
    
    @Column(name = "direccion", length = 100, nullable = false)
    private String direccion;
    
    @Column(name = "ciudad", length = 30, nullable = false)
    private String ciudad;
    
    @Column(name = "telefono", length = 20, nullable = false)
    private BigDecimal telefono;
    
	@ManyToMany(cascade = { 
	    CascadeType.PERSIST, 
	    CascadeType.MERGE
    })
    @JoinTable(name = "cliente_asesor",
    joinColumns = @JoinColumn(name = "cliente_id"),
    inverseJoinColumns = @JoinColumn(name = "asesor_id")
    )
    private Set<Asesor> asesores;

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public BigDecimal getTelefono() {
		return telefono;
	}

	public void setTelefono(BigDecimal telefono) {
		this.telefono = telefono;
	}

	public Set<Asesor> getAsesores() {
		return asesores;
	}

	public void setAsesores(Set<Asesor> asesores) {
		this.asesores = asesores;
	}


}
