package com.co.ibm;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.co.ibm.dao.AsesorDAO;
import com.co.ibm.dao.ClienteDAO;
import com.co.ibm.dao.ConsumoDAO;
import com.co.ibm.dao.PersonDAO;
import com.co.ibm.dao.TarjetaDAO;
import com.co.ibm.entity.Asesor;
import com.co.ibm.entity.Cliente;
import com.co.ibm.entity.Consumo;
import com.co.ibm.entity.Person;
import com.co.ibm.entity.Tarjeta;

@Component
public class DataInit implements ApplicationRunner {
	private PersonDAO personDAO;
	private ClienteDAO clienteDAO;
	private AsesorDAO asesorDAO;
	private TarjetaDAO tarjetaDAO;
	private ConsumoDAO consumoDAO;
	private ArrayList<Tarjeta> tarjetas = new ArrayList();
	private ArrayList<Cliente> clientes = new ArrayList<>();
	 
    private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
 
    @Autowired
    public DataInit(PersonDAO personDAO, ClienteDAO clienteDAO, AsesorDAO asesorDAO,
    		TarjetaDAO tarjetaDAO, ConsumoDAO consumoDAO) {
        this.personDAO = personDAO;
        this.clienteDAO = clienteDAO;
        this.asesorDAO = asesorDAO;
        this.tarjetaDAO = tarjetaDAO;
        this.consumoDAO = consumoDAO;
    }
 
    @Override
    public void run(ApplicationArguments args) throws Exception {
        long count = personDAO.count();
 
        if (count == 0) {
            Person p1 = new Person();
 
            p1.setFullName("John");
 
            Date d1 = df.parse("1980-12-20");
            p1.setDateOfBirth(d1);

            Person p2 = new Person();
 
            p2.setFullName("Smith");
            Date d2 = df.parse("1985-11-11");
            p2.setDateOfBirth(d2);
 
            personDAO.save(p1);
            personDAO.save(p2);
        }
  
        asesorDAO.saveAll(crearAsesores());
        clienteDAO.saveAll(crearClientes());
        tarjetaDAO.saveAll(crearTarjetas());
        consumoDAO.saveAll(crearConsumos());
    }
    
    private Iterable<Consumo> crearConsumos() {
    	Consumo consumo1 = new Consumo();
    	consumo1.setFecha(new Date());
    	consumo1.setDescripcion("Compra 1");
    	consumo1.setMonto(new BigDecimal("10000"));
    	consumo1.setTarjeta(this.tarjetas.get(0));

    	this.consumoDAO.save(consumo1);
    	
    	
    	Consumo consumo2 = new Consumo();
    	consumo2.setFecha(new Date());
    	consumo2.setDescripcion("Compra 2");
    	consumo2.setMonto(new BigDecimal("8000000"));
    	consumo2.setTarjeta(this.tarjetas.get(0));
    	
    	Consumo consumo3 = new Consumo();
    	consumo3.setFecha(new Date());
    	consumo3.setDescripcion("Compra 3");
    	consumo3.setMonto(new BigDecimal("750000"));
    	consumo3.setTarjeta(this.tarjetas.get(1));
    	
    	Consumo consumo4 = new Consumo();
    	consumo4.setFecha(new Date());
    	consumo4.setDescripcion("Compra 4");
    	consumo4.setMonto(new BigDecimal("10700"));
    	consumo4.setTarjeta(this.tarjetas.get(3));
    	
    	Consumo consumo5 = new Consumo();
    	consumo5.setFecha(new Date());
    	consumo5.setDescripcion("Compra 5");
    	consumo5.setMonto(new BigDecimal("100000"));
    	consumo5.setTarjeta(this.tarjetas.get(3));
    	
    	HashSet<Consumo> consumos = new HashSet<>();
    	
    	consumos.add(consumo1);
    	consumos.add(consumo2);
    	consumos.add(consumo3);
    	consumos.add(consumo4);
    	consumos.add(consumo5);
    	
    	return consumos;
    }
    
    private Iterable<Tarjeta> crearTarjetas() {
    	Tarjeta tarjeta1 = new Tarjeta();
    	tarjeta1.setNumeroTarjeta("1234567891123450");
    	tarjeta1.setCcv(123);
    	tarjeta1.setTipo("Credito");
    	tarjeta1.setCliente(this.clientes.get(0));
    	
    	Tarjeta tarjeta2 = new Tarjeta();
    	tarjeta2.setNumeroTarjeta("1234567891123451");
    	tarjeta2.setCcv(123);
    	tarjeta2.setTipo("Debito");
    	tarjeta2.setCliente(this.clientes.get(0));
    	
    	Tarjeta tarjeta3 = new Tarjeta();
    	tarjeta3.setNumeroTarjeta("1234567891123452");
    	tarjeta3.setCcv(123);
    	tarjeta3.setTipo("Credito");
    	tarjeta3.setCliente(this.clientes.get(1));
    	
    	Tarjeta tarjeta4 = new Tarjeta();
    	tarjeta4.setNumeroTarjeta("1234567891123453");
    	tarjeta4.setCcv(123);
    	tarjeta4.setTipo("Debito");
    	tarjeta4.setCliente(this.clientes.get(2));
    	
    	Tarjeta tarjeta5 = new Tarjeta();
    	tarjeta5.setNumeroTarjeta("1234567891123454");
    	tarjeta5.setCcv(123);
    	tarjeta5.setTipo("Credito");
    	tarjeta5.setCliente(this.clientes.get(4));
    	
    	tarjetas.add(tarjeta1);
    	tarjetas.add(tarjeta2);
    	tarjetas.add(tarjeta3);
    	tarjetas.add(tarjeta4);
    	tarjetas.add(tarjeta5);
    	
    	return tarjetas;
    }
    
    private Iterable<Cliente> crearClientes() {
    	Cliente cliente1 = new Cliente();
    	cliente1.setNombre("Cristiano Ronaldo");
    	cliente1.setCiudad("Medellin");
    	cliente1.setDireccion("calle falsa 123");
    	cliente1.setTelefono(new BigDecimal("2545454"));
    	
    	Cliente cliente2 = new Cliente();
    	cliente2.setNombre("Lionel Messi");
    	cliente2.setCiudad("Itagui");
    	cliente2.setDireccion("calle falsa 456");
    	cliente2.setTelefono(new BigDecimal("3195878787"));
    	
    	Cliente cliente3 = new Cliente();
    	cliente3.setNombre("James Rodriguez");
    	cliente3.setCiudad("Bogota");
    	cliente3.setDireccion("calle falsa 478");
    	cliente3.setTelefono(new BigDecimal("3195878780"));
    	
    	Cliente cliente4 = new Cliente();
    	cliente4.setNombre("Milton");
    	cliente4.setCiudad("Medellin");
    	cliente4.setDireccion("calle falsa 123");
    	cliente4.setTelefono(new BigDecimal("5885858"));
    	
    	Cliente cliente5 = new Cliente();
    	cliente5.setNombre("Milton");
    	cliente5.setCiudad("Medellin");
    	cliente5.setDireccion("calle falsa 123");
    	cliente5.setTelefono(new BigDecimal("2565656"));
    	
    	clientes.add(cliente1);
    	clientes.add(cliente2);
    	clientes.add(cliente3);
    	clientes.add(cliente4);
    	clientes.add(cliente5);
    	
    	return clientes;
    }
    
    private Iterable<Asesor> crearAsesores() {
    	Asesor asesor1 = new Asesor();
    	asesor1.setNombre("Zidane");
    	asesor1.setEspecialidad("Sonido");
    	
    	Asesor asesor2 = new Asesor();
    	asesor2.setNombre("Rafael Benitez");
    	asesor2.setEspecialidad("Televisores");
    	
    	Asesor asesor3 = new Asesor();
    	asesor3.setNombre("Simeone");
    	asesor3.setEspecialidad("Celulares");
    	
    	Asesor asesor4 = new Asesor();
    	asesor4.setNombre("Mourinho");
    	asesor4.setEspecialidad("Rejojes");
    	
    	Asesor asesor5 = new Asesor();
    	asesor5.setNombre("Guardionla");
    	asesor5.setEspecialidad("Neveras");
    	
    	HashSet<Asesor> asesores = new HashSet<>();
    	
    	asesores.add(asesor1);
    	asesores.add(asesor2);
    	asesores.add(asesor3);
    	asesores.add(asesor4);
    	asesores.add(asesor5);
    	
    	return asesores;
    }
}
