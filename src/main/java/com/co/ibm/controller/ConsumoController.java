package com.co.ibm.controller;

import java.util.Date;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.co.ibm.dao.ConsumoDAO;
import com.co.ibm.dao.TarjetaDAO;
import com.co.ibm.entity.Consumo;
import com.co.ibm.entity.Tarjeta;
import com.co.ibm.request_model.ConsumoRequest;
import com.co.ibm.transformer.ConsumoTransformer;

@CrossOrigin
@Controller
public class ConsumoController {
	@Autowired
    private ConsumoDAO consumoDAO;
	@Autowired
    private TarjetaDAO tarjetaDAO;
	
	@ResponseBody
    @RequestMapping("/consumo")
    public ResponseEntity<Object> tarjetaes() {
    	try {
    		return new ResponseEntity <Object> (consumoDAO.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <Object> ("Error tecnico", HttpStatus.INTERNAL_SERVER_ERROR);
		}
        
    }
    
    @ResponseBody
    @RequestMapping("/consumo/{id}")
    public ResponseEntity<Object> consumo(@PathVariable("id") Long idConsumo) {
    	if(idConsumo == null) {
    		return new ResponseEntity <Object> ("No se admite un id vacio", HttpStatus.BAD_REQUEST);
    	}
    	try {
    		Consumo consumo = consumoDAO.findById(idConsumo).orElse(null);
    		return new ResponseEntity <Object> (consumo, consumo == null ? HttpStatus.NO_CONTENT : HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <Object> ("Error tecnico", HttpStatus.INTERNAL_SERVER_ERROR);
		}    	
    }
    
    @ResponseBody
    @RequestMapping("/consumo/{numeroTarjeta}/tarjeta")
    public Object updateClientes(@PathVariable("numeroTarjeta") String numeroTarjeta) { 
    	if(numeroTarjeta == null) {
    		return new ResponseEntity <Object> ("El numero de tarjeta no puede ser vacio", HttpStatus.BAD_REQUEST);
    	}
    	try {
    		Tarjeta tarjeta = tarjetaDAO.findById(numeroTarjeta).orElse(null);
    		if(tarjeta == null) {
    			return new ResponseEntity <> ("No se encontro una tarjeta con el numero: " + numeroTarjeta, HttpStatus.OK);
    		}
    		Set<ConsumoRequest> consumoRequest = ConsumoTransformer.getConsumoResponse(consumoDAO.findByTarjeta(tarjeta));
    		return new ResponseEntity <Object> (consumoRequest, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <Object> ("error Tecnico", HttpStatus.INTERNAL_SERVER_ERROR);
		}    	
    }
    
    @ResponseBody
    @PostMapping("/consumo")
    public ResponseEntity<Object> settarjetas(@RequestBody ConsumoRequest consumoRequest) {
    	if(consumoRequest == null) {
    		return new ResponseEntity <Object> ("No se admiten peticiones sin datos", HttpStatus.BAD_REQUEST);
    	}    	
		try {
			Tarjeta tarjeta = tarjetaDAO.findById(consumoRequest.getNumeroTarjeta()).orElse(null);
			if(tarjeta == null) {
				return new ResponseEntity <Object> ("No se encuentran datos para el número de tarjeta ", HttpStatus.OK);
			}
			Consumo consumo = ConsumoTransformer.consumoRequestToConsumo(consumoRequest, tarjeta);
			consumoDAO.save(consumo);
			
			return new ResponseEntity <Object> (ConsumoTransformer.getConsumoResponse(consumoDAO.findAll()), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <Object> ("Error tecnico", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
    }
    
    @ResponseBody
    @DeleteMapping("/consumo")
    public ResponseEntity <Object> deletePerson(@RequestBody Long idConsumo) {
    	if(idConsumo == null || idConsumo <= 0) {
    		return new ResponseEntity <Object> ("Datos vacios", HttpStatus.BAD_REQUEST);
    	}
    	try {
    		Consumo consumo = consumoDAO.findById(idConsumo).orElse(null);
    		
    		if(consumo == null) {
    			return new ResponseEntity <Object> ("No existe el consumo", HttpStatus.NO_CONTENT);
    		}    		
    		consumoDAO.deleteById(idConsumo);
    		return new ResponseEntity <Object> (consumoDAO.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <Object> ("Error de datos", HttpStatus.INTERNAL_SERVER_ERROR);
		}
        
    }
}
