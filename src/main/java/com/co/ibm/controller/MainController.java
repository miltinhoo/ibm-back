package com.co.ibm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.co.ibm.dao.ClienteDAO;
import com.co.ibm.dao.PersonDAO;
import com.co.ibm.entity.Cliente;
import com.co.ibm.entity.Person;

@CrossOrigin
@Controller
public class MainController {
	@Autowired
    private PersonDAO personDAO;
	@Autowired
    private ClienteDAO clienteDAO;
 
    @ResponseBody
    @RequestMapping("/")
    public String index() {
        Iterable<Person> all = personDAO.findAll();
 
        StringBuilder sb = new StringBuilder();
 
        all.forEach(p -> sb.append(p.getFullName() + "<br>"));
 
        return sb.toString();
    }
    
    @ResponseBody
    @RequestMapping("/cliente")
    public ResponseEntity<Iterable<Cliente>> clientes() {
    	try {
    		return new ResponseEntity <Iterable<Cliente>> (clienteDAO.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <> (null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
        
    }
    
    @ResponseBody
    @RequestMapping("/cliente/{id}")
    public ResponseEntity<Cliente> cliente(@PathVariable("id") Long idCliente) {
    	if(idCliente == null) {
    		return new ResponseEntity <> (null, HttpStatus.BAD_REQUEST);
    	}
    	try {
    		Cliente cliente = clienteDAO.findById(idCliente).orElse(null);
    		return new ResponseEntity <Cliente> (cliente, cliente == null ? HttpStatus.NO_CONTENT : HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <> (null, HttpStatus.INTERNAL_SERVER_ERROR);
		}    	
    }
    /*
    @ResponseBody
    @RequestMapping("/cliente/id")
    public String updateClientes(@RequestParam String personId) { 
        return personId;
    }*/
    
    @ResponseBody
    @PostMapping("/cliente")
    public ResponseEntity<Object> setClientes(@RequestBody Cliente cliente) {
    	if(cliente == null) {
    		return new ResponseEntity <Object> (null, HttpStatus.BAD_REQUEST);
    	}    	
		try {
			Cliente clienteDao = clienteDAO.findById(cliente.getIdCliente()).orElse(null);
			if(clienteDao == null) {
				return new ResponseEntity <Object> ("Ya existe otro cliente con el mismo id", HttpStatus.OK);
			}
			clienteDAO.save(cliente);
			return new ResponseEntity <Object> (clienteDAO.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <Object> (null, HttpStatus.INTERNAL_SERVER_ERROR);
		}		
    }
    
    @ResponseBody
    @PatchMapping("/cliente")
    public ResponseEntity<Iterable<Cliente> > updateClientes(@RequestBody Cliente cliente) {
    	if(cliente == null) {
    		return new ResponseEntity <> (null, HttpStatus.BAD_REQUEST);
    	}    	
		try {
			clienteDAO.save(cliente);
			return new ResponseEntity <Iterable<Cliente>> (clienteDAO.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <> (null, HttpStatus.INTERNAL_SERVER_ERROR);
		}		
    }
    
    @ResponseBody
    @DeleteMapping("/cliente/{idCliente}")
    public ResponseEntity <Object> deletePerson(@PathVariable("idCliente") Long idCliente) {
    	if(idCliente == null || idCliente <= 0) {
    		return new ResponseEntity <Object> ("Datos invalidos", HttpStatus.BAD_REQUEST);
    	}
    	try {
    		Cliente cliente = clienteDAO.findById(idCliente).orElse(null);
    		
    		if(cliente == null) {
    			return new ResponseEntity <Object> ("Cliente no existe", HttpStatus.NO_CONTENT);
    		}    		
    		clienteDAO.deleteById(idCliente);
    		return new ResponseEntity <Object> (clienteDAO.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <Object> ("Error", HttpStatus.INTERNAL_SERVER_ERROR);
		}
        
    }    
}
