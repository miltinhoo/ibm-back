package com.co.ibm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.co.ibm.dao.AsesorDAO;
import com.co.ibm.entity.Asesor;

@CrossOrigin
@Controller
public class AsesorController {
	@Autowired
    private AsesorDAO asesorDAO;
	
	@ResponseBody
    @RequestMapping("/asesor")
    public ResponseEntity<Iterable<Asesor>> asesores() {
    	try {
    		return new ResponseEntity <Iterable<Asesor>> (asesorDAO.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <> (null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
        
    }
    
    @ResponseBody
    @RequestMapping("/asesor/{id}")
    public ResponseEntity<Asesor> asesor(@PathVariable("id") Long idAsesor) {
    	if(idAsesor == null) {
    		return new ResponseEntity <> (null, HttpStatus.BAD_REQUEST);
    	}
    	try {
    		Asesor asesor = asesorDAO.findById(idAsesor).orElse(null);
    		return new ResponseEntity <Asesor> (asesor, asesor == null ? HttpStatus.NO_CONTENT : HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <> (null, HttpStatus.INTERNAL_SERVER_ERROR);
		}    	
    }
    
    @ResponseBody
    @PostMapping("/asesor")
    public ResponseEntity<Iterable<Asesor> > setasesors(@RequestBody Asesor asesor) {
    	if(asesor == null) {
    		return new ResponseEntity <> (null, HttpStatus.BAD_REQUEST);
    	}    	
		try {
			asesorDAO.save(asesor);
			return new ResponseEntity <Iterable<Asesor>> (asesorDAO.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <> (null, HttpStatus.INTERNAL_SERVER_ERROR);
		}		
    }
    
    @ResponseBody
    @DeleteMapping("/asesor/{idAsesor}")
    public ResponseEntity <Object> deletePerson(@PathVariable("idAsesor") Long idAsesor) {
    	if(idAsesor == null || idAsesor <= 0) {
    		return new ResponseEntity <Object> ("Datos invalidos", HttpStatus.BAD_REQUEST);
    	}
    	try {
    		Asesor asesor = asesorDAO.findById(idAsesor).orElse(null);
    		
    		if(asesor == null) {
    			return new ResponseEntity <Object> ("asesor no existe", HttpStatus.NO_CONTENT);
    		}    		
    		asesorDAO.deleteById(idAsesor);
    		return new ResponseEntity <Object> (asesorDAO.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <Object> ("Error", HttpStatus.INTERNAL_SERVER_ERROR);
		}
        
    }
}
