package com.co.ibm.controller;

import java.util.Date;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.co.ibm.dao.ClienteDAO;
import com.co.ibm.dao.TarjetaDAO;
import com.co.ibm.entity.Cliente;
import com.co.ibm.entity.Tarjeta;
import com.co.ibm.request_model.TarjetaRequest;
import com.co.ibm.transformer.TarjetaTransformer;

@CrossOrigin
@Controller
public class TarjetaController {
	@Autowired
    private TarjetaDAO tarjetaDAO;
	@Autowired
    private ClienteDAO clienteDAO;
	
	@ResponseBody
    @RequestMapping("/tarjeta")
    public ResponseEntity<Iterable<Tarjeta>> tarjetaes() {
    	try {
    		return new ResponseEntity <Iterable<Tarjeta>> (tarjetaDAO.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <> (null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
        
    }
    
    @ResponseBody
    @RequestMapping("/tarjeta/{idCiente}")
    public ResponseEntity<Set<TarjetaRequest>> tarjeta(@PathVariable("idCiente") Long idCliente) {
    	if(idCliente == null) {
    		return new ResponseEntity <> (null, HttpStatus.BAD_REQUEST);
    	}
    	try {
    		Cliente cliente = clienteDAO.findById(idCliente).orElse(null);
    		if(cliente == null) {
    			return new ResponseEntity <> (null, HttpStatus.NO_CONTENT);
    		}
    		Set<TarjetaRequest> tarjetas = TarjetaTransformer.getTarjetaResponse(tarjetaDAO.findByCliente(cliente));
    		return new ResponseEntity <Set<TarjetaRequest>> (tarjetas, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <> (null, HttpStatus.INTERNAL_SERVER_ERROR);
		}    	
    }
    
    @ResponseBody
    @PostMapping("/tarjeta")
    public ResponseEntity<Object> setTarjetas(@RequestBody TarjetaRequest tarjetaRequest) {
    	if(tarjetaRequest == null) {
    		return new ResponseEntity <Object> ("Bad request", HttpStatus.BAD_REQUEST);
    	}    	
		try {
			if(tarjetaDAO.existsById(tarjetaRequest.getNumeroTarjeta()) == true) {
				return new ResponseEntity <Object> ("No ha sido posible crear la tarjeta debido a que ya existe otra con el mimso número", HttpStatus.OK);
			}			
			return new ResponseEntity <Object> (insertarRegistroTarjeta(tarjetaRequest), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <Object> ("Error tecnico", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
    }
    
    @ResponseBody
    @DeleteMapping("/tarjeta")
    public ResponseEntity <String> deletePerson(@RequestBody String idTarjeta) {
    	if(idTarjeta == null || idTarjeta.length() > 0) {
    		return new ResponseEntity <String> ("Datos invalidos", HttpStatus.BAD_REQUEST);
    	}
    	try {
    		Tarjeta tarjeta = tarjetaDAO.findById(idTarjeta).orElse(null);
    		
    		if(tarjeta == null) {
    			return new ResponseEntity <String> ("tarjeta no existe", HttpStatus.NO_CONTENT);
    		}    	
    		tarjetaDAO.deleteById(idTarjeta);
    		return new ResponseEntity <String> ("OK", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <String> ("Error", HttpStatus.INTERNAL_SERVER_ERROR);
		}
        
    }
    
    @ResponseBody
    @PatchMapping("/tarjeta")
    public ResponseEntity<Object> updateTarjetas(@RequestBody TarjetaRequest tarjetaRequest) {
    	if(tarjetaRequest == null) {
    		return new ResponseEntity <Object> ("Bad request", HttpStatus.BAD_REQUEST);
    	}    	
		try {
			return new ResponseEntity <Object> (insertarRegistroTarjeta(tarjetaRequest), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity <Object> ("Error tecnico", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
    }
    
    private Set<TarjetaRequest> insertarRegistroTarjeta(TarjetaRequest tarjetaRequest) {
    	Cliente cliente = clienteDAO.findById(tarjetaRequest.getIdCliente()).orElse(null);
		Tarjeta tarjeta = TarjetaTransformer.tarjetaRequestToTarjeta(tarjetaRequest, cliente);
		tarjetaDAO.save(tarjeta);
		return TarjetaTransformer.getTarjetaResponse(tarjetaDAO.findAll());		
    }
}
