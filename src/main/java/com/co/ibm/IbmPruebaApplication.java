package com.co.ibm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IbmPruebaApplication {

	public static void main(String[] args) {
		SpringApplication.run(IbmPruebaApplication.class, args);
	}
}
