package com.co.ibm.dao;

import org.springframework.data.repository.CrudRepository;

import com.co.ibm.entity.Asesor;

public interface AsesorDAO extends CrudRepository<Asesor, Long>   {

}
