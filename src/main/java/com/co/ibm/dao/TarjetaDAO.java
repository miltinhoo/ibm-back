package com.co.ibm.dao;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.co.ibm.entity.Cliente;
import com.co.ibm.entity.Tarjeta;

public interface TarjetaDAO extends CrudRepository<Tarjeta, String>   {
	public Set<Tarjeta> findByCliente(Cliente cliente);
}
