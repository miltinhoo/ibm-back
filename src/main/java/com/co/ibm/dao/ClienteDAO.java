package com.co.ibm.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.co.ibm.entity.Cliente;


public interface ClienteDAO extends CrudRepository<Cliente, Long>  {
}
