package com.co.ibm.dao;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.co.ibm.entity.Consumo;
import com.co.ibm.entity.Tarjeta;

public interface ConsumoDAO extends CrudRepository<Consumo, Long>   {
	public Set<Consumo> findByTarjeta(Tarjeta tarjeta);

}
